import 'package:http/http.dart' as http;
import 'dart:convert';

const List<String> currenciesList = [
  'AUD',
  'BRL',
  'CAD',
  'CNY',
  'EUR',
  'GBP',
  'HKD',
  'IDR',
  'ILS',
  'INR',
  'JPY',
  'MXN',
  'NOK',
  'NZD',
  'PLN',
  'RON',
  'RUB',
  'SEK',
  'SGD',
  'USD',
  'ZAR'
];

const List<String> cryptoList = [
  'BTC',
  'ETH',
  'LTC',
];

const String key = '8589B49C-BBA6-4771-B87E-376C43DF9C5C';

class CoinData {
  Future getCoinData(String currency) async {
    Map<String, String> cryptoValues = <String, String>{};
    Map<String, String> headers = {'X-CoinAPI-Key': key};
    for (String crypto in cryptoList) {
      Uri url =
          Uri.https('rest.coinapi.io', '/v1/exchangerate/$crypto/$currency');
      http.Response response = await http.get(url, headers: headers);
      if (response.statusCode == 200) {
        String data = response.body;
        print(data);

        var decodedData = jsonDecode(data);
        double rate = decodedData['rate'];

        cryptoValues[crypto] = rate.toStringAsFixed(0);
      } else {
        print(response.statusCode);
      }
    }

    return cryptoValues;
  }
}
