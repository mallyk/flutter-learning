import 'package:flutter/material.dart';
import 'coin_data.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io' show Platform;

class PriceScreen extends StatefulWidget {
  @override
  _PriceScreenState createState() => _PriceScreenState();
}

class _PriceScreenState extends State<PriceScreen> {
  String selectedCurrency = 'USD';
  Map<String, String> coinValues = {};
  List<Widget> bodyWidgets = [];

  @override
  void initState() {
    super.initState();

    bodyWidgets = createCards();
    bodyWidgets.add(createCurrencyPicker());
    getExchangeRate(selectedCurrency);
  }

  Future getExchangeRate(String currency) async {
    CoinData coinData = CoinData();
    Map<String, String> coinDataResults = await coinData.getCoinData(currency);

    setState(() {
      coinValues = coinDataResults;
    });
  }

  DropdownButton getAndroidDropDown() {
    List<DropdownMenuItem<String>> dropdownItems = [];
    for (String currency in currenciesList) {
      var newItem = DropdownMenuItem(
        child: Text(currency),
        value: currency,
      );

      dropdownItems.add(newItem);
    }

    return DropdownButton<String>(
      value: selectedCurrency,
      items: dropdownItems,
      onChanged: (value) {
        setState(() {
          if (value != null) {
            selectedCurrency = value;
            getExchangeRate(value);
          }
        });
      },
    );
  }

  CupertinoPicker getIosPicker() {
    List<Text> pickerItems = [];
    for (String currency in currenciesList) {
      pickerItems.add(Text(currency));
    }

    return CupertinoPicker(
      backgroundColor: Colors.lightBlue,
      itemExtent: 32.0,
      onSelectedItemChanged: (selectedIndex) {
        print(selectedIndex);
      },
      children: pickerItems,
    );
  }

  Widget getPicker() {
    if (Platform.isIOS) {
      return getIosPicker();
    } else {
      return getAndroidDropDown();
    }
  }

  List<Widget> createCards() {
    List<Widget> cards = [];
    for (String crypto in cryptoList) {
      CryptoCard card = CryptoCard(
          coinType: crypto,
          exchangeRate: coinValues[crypto] ?? '?',
          selectedCurrency: selectedCurrency);
      cards.add(card);
    }
    return cards;
  }

  Container createCurrencyPicker() {
    return Container(
      height: 150.0,
      alignment: Alignment.center,
      padding: EdgeInsets.only(bottom: 30.0),
      color: Colors.lightBlue,
      child: Platform.isIOS ? getIosPicker() : getAndroidDropDown(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('🤑 Coin Ticker'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          for (String crypto in cryptoList)
            CryptoCard(
              coinType: crypto,
              exchangeRate: coinValues[crypto] ?? '?',
              selectedCurrency: selectedCurrency,
            ),
          Container(
            height: 150.0,
            alignment: Alignment.center,
            padding: EdgeInsets.only(bottom: 30.0),
            color: Colors.lightBlue,
            child: Platform.isIOS ? getIosPicker() : getAndroidDropDown(),
          ),
        ],
      ),
    );
  }
}

class CryptoCard extends StatelessWidget {
  const CryptoCard({
    required this.coinType,
    required this.exchangeRate,
    required this.selectedCurrency,
  });

  final String coinType;
  final String exchangeRate;
  final String selectedCurrency;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 0),
      child: Card(
        color: Colors.lightBlueAccent,
        elevation: 5.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 28.0),
          child: Text(
            '1 $coinType = $exchangeRate $selectedCurrency',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 20.0,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
