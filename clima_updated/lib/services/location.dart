import 'package:geolocator/geolocator.dart';

class Location {
  double latitude = 0.0;
  double longitude = 0.0;

  Future<void> getLocation() async {
    try {
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.low);
      print('hello');

      latitude = position.latitude;
      longitude = position.longitude;
      print('got stuff');
    } on Exception catch (e) {
      print(e);
    }
  }
}
